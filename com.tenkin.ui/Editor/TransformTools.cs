﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TransformTools 
{
   /// <summary>
   /// 找到某个组件transform路径
   /// </summary>
    [MenuItem("GameObject/UI/GetTransformPath", false, 10)]
    public static void CreatHotViewWindow()
    {
        Transform transform = Selection.activeTransform;
        string name = Path(transform, transform.gameObject.name);
        name= name.Replace($"{transform.root.name}/", "");
        Debug.LogError(name);

       string content=$"transform.root.Find(\"{name}\");";
        GUIUtility.systemCopyBuffer = name;//调用系统的ctrl+c
    }
    private static string Path(Transform transform,string name) {
        string tmp = name;
        if (transform.parent != null)
        {
            tmp = $"{transform.parent.name}/{tmp}";

            return Path(transform.parent, tmp);
        }
        else
        {
            return tmp;
        }
    }
}
