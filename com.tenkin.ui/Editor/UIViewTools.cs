﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace UIViewEditor
{
    using static UIViewTools;
    using static UIViewRule;
    public class UIViewWinows : EditorWindow
    {
        public static string aimPath {
            set { EditorPrefs.SetString("HotUIViewAimPath", value); }
            get { return EditorPrefs.GetString("HotUIViewAimPath"); }

        }
        public static UIViewWinows Instance;
        public static void CreatUIViewWindows()
        {
            Instance = EditorWindow.CreateWindow<UIViewWinows>("Title");

            Instance.stringBuilder = new StringBuilder();
            Instance.mgrSb = new StringBuilder();
            Instance.dataSb = new StringBuilder();
            Instance.RunAllCreatCmd();
        }
        float angle;
        StringBuilder stringBuilder;
        Vector2 scriptScrollView;
        Vector2 guiScrollView;

        StringBuilder mgrSb, dataSb;
        

        private void OnGUI()
        {

            GUILayout.BeginHorizontal();
            GUILayout.Label($"UI目标路径:{aimPath}");
            if (GUILayout.Button("设置路径"))
            {
                aimPath = EditorUtility.OpenFolderPanel("文件信息", Application.dataPath, "UI");
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            GUILayout.Label("InterfaceName", GUILayout.Width(100));
            UIViewTools.InterfaceName = GUILayout.TextField(UIViewTools.InterfaceName, GUILayout.Width(200));

            GUILayout.Label("UIType", GUILayout.Width(100));
            UIViewTools.UIType = (UIType)EditorGUILayout.EnumPopup(UIViewTools.UIType, GUILayout.Width(200));

            GUILayout.Label("继承UIViewBase", GUILayout.Width(100));
            UIViewTools.isFromUIviewbase = EditorGUILayout.Toggle(UIViewTools.isFromUIviewbase, GUILayout.Width(200));
            GUILayout.EndHorizontal();

            GUILayout.Space(40);
            //编辑器
            guiScrollView = GUILayout.BeginScrollView(guiScrollView);

            for (int i = 0; i < UIViewTools.ViewCompList.Count; i++)
            {
                GUI.color = ViewCompList[i].isRepeatName == true ? Color.red : Color.white;

                GUILayout.BeginHorizontal();

                GUILayout.Label("创建", GUILayout.Width(40));
                ViewCompList[i].isCreat = GUILayout.Toggle(ViewCompList[i].isCreat, "", GUILayout.MaxWidth(40));


                GUILayout.Label("事件", GUILayout.Width(40));
                ViewCompList[i].isUiEvent = GUILayout.Toggle(ViewCompList[i].isUiEvent, "", GUILayout.MaxWidth(40));

                GUILayout.Label("动画", GUILayout.Width(40));
                ViewCompList[i].isAnimator = GUILayout.Toggle(ViewCompList[i].isAnimator, "", GUILayout.MaxWidth(40));

                //GUILayout.TextArea("类型", GUILayout.Width(40));
                GUILayout.TextField(UIViewTools.ViewCompList[i].TypeName, GUILayout.Width(200));
                GUILayout.Label("路径", GUILayout.Width(40));
                GUILayout.TextField(UIViewTools.ViewCompList[i].Path, GUILayout.Width(200));
                //GUILayout.TextArea("组件", GUILayout.Width(40));
                //EditorGUILayout.ObjectField(UIViewTools.ViewCompList[i].CompType, typeof(Object), GUILayout.Width(200));

                GUILayout.Label("命名", GUILayout.Width(40));
                GUILayout.TextField($"{ViewCompList[i].Name}", GUILayout.Width(200));

                GUILayout.Label("物体", GUILayout.Width(40));
                EditorGUILayout.ObjectField(ViewCompList[i].mGameObject,typeof(GameObject), GUILayout.Width(200));


                if (ViewCompList[i].isUiEvent)
                {
                    UIViewTools.ViewCompList[i].EventFuncName = GUILayout.TextField(UIViewTools.ViewCompList[i].EventFuncName, GUILayout.Width(200));
                }


                GUILayout.EndHorizontal();
            }
            GUILayout.EndScrollView();

            if (GUI.changed)
            {

                RunAllCreatCmd();
            }


            if (GUILayout.Button("写入"))
            {
                string content = stringBuilder.ToString();

                if (string.IsNullOrEmpty(aimPath)==false)
                {

                    string filPath = $"{aimPath}/{UIViewTools.Name}View.cs";
                    Debug.Log("生成" + filPath);
                    using (StreamWriter writer = new StreamWriter(filPath))
                    {
                        writer.Write(content);
                        AssetDatabase.Refresh();
                    }
                }
                else {
                    EditorUtility.DisplayDialog("目标文件夹不能为空", "目标文件夹不能为空", "好");
                }
            }


            if (GUILayout.Button("重置"))
            {
                CreatHotViewScript();
                RunAllCreatCmd();
            }



            GUI.color = Color.white;
            if (stringBuilder != null)
            {
                scriptScrollView = GUILayout.BeginScrollView(scriptScrollView);
                GUILayout.TextArea(stringBuilder.ToString());
                GUILayout.EndScrollView();
            }

            if (GUILayout.Button("创建模块,自动生成管理器模板代码，数据模板代码"))
            {
                CreateDataScript();
                CreateModuleScript();

            }
        }

        private void CreateModuleScript()
        {
           
            mgrSb.AppendLine($"using UIFramework;");
            mgrSb.AppendLine($"using UnityEngine;");
            mgrSb.AppendLine($"using UnityEngine.UI;");
            mgrSb.AppendLine($"namespace UIModule");
            mgrSb.AppendLine($"\t{{");
            mgrSb.AppendLine($"\tpublic  class {UIViewTools.Name}Mgr  : UIMgrBase,I{UIViewTools.Name}CallBack {{");
            mgrSb.AppendLine($"\tprivate   {UIViewTools.Name}View view;");
            mgrSb.AppendLine($"\tprivate  GameObject go;");
            mgrSb.AppendLine($"\tprivate  UIMgr ui_mgr;");
            mgrSb.AppendLine($"\tprivate  {UIViewTools.Name}Data data;");
            mgrSb.AppendLine($"\tpublic override void OnEnter(string preState)");
            mgrSb.AppendLine($"\t{{");
            mgrSb.AppendLine($"       this.view = new {UIViewTools.Name}View(this, go, ui_mgr);");
            mgrSb.AppendLine($"\t}}");
            mgrSb.AppendLine($"\tpublic override void OnExit(string nextState, bool isDestroy)");
            mgrSb.AppendLine($"\t{{");
            mgrSb.AppendLine($"        base.OnExit(nextState, isDestroy);");
            mgrSb.AppendLine($"\t}}");
            mgrSb.AppendLine($"\t}}");
            mgrSb.AppendLine($"\t}}");
            string filPath = $"{aimPath}/{UIViewTools.Name}Mgr.cs";
            Debug.Log("生成" + filPath);
            string content = mgrSb.ToString();
            using (StreamWriter writer = new StreamWriter(filPath))
            {
                writer.Write(content);
                AssetDatabase.Refresh();
            }
        }
        void CreateDataScript()
        {
            dataSb.AppendLine($"namespace UIModule");
            dataSb.AppendLine($"\t{{");
            dataSb.AppendLine($"\tpublic  class {UIViewTools.Name}Data  {{");
            dataSb.AppendLine($"\t}}");
            dataSb.AppendLine($"\t}}");
            string filePath = $"{aimPath}/{UIViewTools.Name}Data.cs";
            Debug.Log("生成" + filePath);
            string content = dataSb.ToString();
            WriteScript(filePath,content);
        }

        void WriteScript(string filePath,string content)
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.Write(content);
                AssetDatabase.Refresh();
            }
        }
        //主入口执行所有创建方法
        private void RunAllCreatCmd()
        {
            if (stringBuilder != null)
                stringBuilder.Clear();
            else
                stringBuilder = new StringBuilder();

            WriteClassStart(stringBuilder, UIViewTools.Name);
            WriteConstructorFunc(stringBuilder, UIViewTools.Name, UIViewTools.ViewCompList);
            WriteDisposeFunc(stringBuilder, UIViewTools.ViewCompList);
            CreatClassEnd(stringBuilder);
            
        } 
        private void WriteClassStart(StringBuilder stringBuild, string name)
        {

            stringBuild.AppendLine($"using UIFramework;");
            stringBuild.AppendLine($"using UnityEngine;");
            stringBuild.AppendLine($"using UnityEngine.UI;");
           // stringBuild.AppendLine($"using UIModule;");
            if (!string.IsNullOrEmpty(UIViewTools.InterfaceName))
                CreatUIEvent(stringBuild);


       

            //继承uibase 的话
            if (UIViewTools.isFromUIviewbase)
            {
                stringBuild.AppendLine($"\tpublic  class {name}View  : UIViewBase {{");

                stringBuild.AppendLine($"\t\tpublic override string PrefabsName => \"{name}.prefab\";");
                stringBuild.AppendLine($"\t\tpublic override UIType UIType => UIType.{UIViewTools.UIType};");
            }
            else {

                stringBuild.AppendLine($"\tpublic  class {name}View {{");
                stringBuild.AppendLine($"\t\tpublic  string PrefabsName => \"{name}.prefab\";");
                stringBuild.AppendLine($"\t\tpublic  GameObject gameObject;");
            }

            stringBuild.AppendLine($"");

            if (!string.IsNullOrEmpty(UIViewTools.InterfaceName)) 
                stringBuild.AppendLine($"\t\tprivate {UIViewTools.InterfaceName} call;");



            for (int i = 0; i < UIViewTools.ViewCompList.Count; i++)
            {
                if (UIViewTools.ViewCompList[i].TypeName.Equals("Animator") && UIViewTools.ViewCompList[i].isAnimator == false)
                    continue;

                if (UIViewTools.ViewCompList[i].isCreat)
                {
                    string aa = RemoveSubstring(ViewCompList[i].Name,$" (Legacy)").ToLower();
                    stringBuild.AppendLine($"\t\tprivate {ViewCompList[i].TypeName} {ViewCompList[i].Name};");
                }    
                    
            }
        }
        public  string RemoveSubstring(string str, string substr)
        {
            return str.Replace(substr, string.Empty);
        }

        //写入摧毁函数
        private void WriteDisposeFunc(StringBuilder stringBuild, List<ViewComp> ViewCompList)
        {
            if (UIViewTools.isFromUIviewbase)
            {
                stringBuild.AppendLine($"\t\tpublic override void Dispose(){{");
            }
            else {
                stringBuild.AppendLine($"\t\tpublic void Dispose(){{");
            }
            if (!string.IsNullOrEmpty(UIViewTools.InterfaceName))
            {
                //删除注册事件
                for (int i = 0; i < UIViewTools.ViewCompList.Count; i++)
                {
                    if (UIViewTools.ViewCompList[i].isCreat)
                        if (UIViewTools.ViewCompList[i].isUiEvent)
                            RegisterUIEvent(stringBuild, UIViewTools.ViewCompList[i], ConvertToEnum(UIViewTools.ViewCompList[i].TypeName), false);
                }

                stringBuild.AppendLine("");
            }

            //清空组件
            for (int i = 0; i < UIViewTools.ViewCompList.Count; i++)
            {
                if (UIViewTools.ViewCompList[i].TypeName.Equals("Animator") && UIViewTools.ViewCompList[i].isAnimator == false)
                    continue;
                if (UIViewTools.ViewCompList[i].isCreat)
                {
                    stringBuild.AppendLine($"\t\t\t{ViewCompList[i].Name} = null;");
                }
            }

            stringBuild.AppendLine($"\t\t\tGameObject.Destroy(this.gameObject);");
            stringBuild.AppendLine($"\t\t}}");

        }
        //写入构造函数
        private void WriteConstructorFunc(StringBuilder stringBuild, string name, List<ViewComp> ViewCompList)
        {
            if (!string.IsNullOrEmpty(UIViewTools.InterfaceName))
                stringBuild.AppendLine($"\t\tpublic {name}View({UIViewTools.InterfaceName} mCall,GameObject go,UIMgr ui_mgr){{");
            else
            {
                if (UIViewTools.isFromUIviewbase)
                {
                    stringBuild.AppendLine($"\t\tpublic {name}View(){{");
                }
                else
                {
                    stringBuild.AppendLine($"\t\tpublic {name}View(Transform mParent){{");
                }
            }

            //创造函数
            if (UIViewTools.isFromUIviewbase)
            {
                stringBuild.AppendLine($"\t\t\tCreatGameObject(UIType,go,ui_mgr);");
            }
            else {
                stringBuild.AppendLine($"\t\t\tgameObject= ResManager.Instance.LoadGo(PrefabsName, typeof(GameObject));");
                stringBuild.AppendLine($"\t\t\tgameObject.transform.SetParent(mParent);");
                stringBuild.AppendLine($"\t\t\tgameObject.transform.localScale=Vector3.one;");
            }



            if (!string.IsNullOrEmpty(UIViewTools.InterfaceName))
                stringBuild.AppendLine($"\t\t\tcall=mCall;");
            //获取组件
            for (int i = 0; i < UIViewTools.ViewCompList.Count; i++)
            {
                if (UIViewTools.ViewCompList[i].isCreat)
                    WriteGetComponent(stringBuilder, UIViewTools.ViewCompList[i]);
            }
            stringBuild.AppendLine("");
            if (!string.IsNullOrEmpty(UIViewTools.InterfaceName))
            {
                //注册事件
                for (int i = 0; i < UIViewTools.ViewCompList.Count; i++)
                {

                    if (UIViewTools.ViewCompList[i].isCreat)
                        if (UIViewTools.ViewCompList[i].isUiEvent)
                            RegisterUIEvent(stringBuild, UIViewTools.ViewCompList[i], ConvertToEnum(UIViewTools.ViewCompList[i].TypeName), true);
                }
            }
            stringBuild.AppendLine($"\t\t}}");
        }
        //获取
        private void WriteGetComponent(StringBuilder stringBuild, ViewComp viewComp)
        {

            //动画且
            if (viewComp.TypeName.Equals("Animator") && viewComp.isAnimator == false)
                return;

                stringBuild.AppendLine($"\t\t\t{viewComp.Name} = gameObject.transform.Find(\"{viewComp.Path}\").GetComponent<{viewComp.TypeName}>();");
        }
        private void CreatClassEnd(StringBuilder stringBuild)
        {
            stringBuild.AppendLine($"\t}}");
            //stringBuild.AppendLine($"}}");
        }
        private void RegisterUIEvent(StringBuilder stringBuild, ViewComp viewComp, UIViewType type, bool Register)
        {
            string tmplistener = Register == true ? "AddListener" : "RemoveListener";

            switch (type)
            {
                case UIViewType.Image:

                    break;
                case UIViewType.RawImage:
                    break;
                case UIViewType.Button:
                    stringBuild.AppendLine($"\t\t\t{viewComp.Name}.onClick.{tmplistener}(this.call.{viewComp.EventFuncName});");
                    break;
                case UIViewType.Toggle:
                    stringBuild.AppendLine($"\t\t\t{viewComp.Name}.onValueChanged.{tmplistener}(this.call.{viewComp.EventFuncName});");

                    break;
                case UIViewType.Slider:
                  //  stringBuild.AppendLine($"\t\t\t{viewComp.Name}.onValueChanged.{tmplistener}(this.call.{viewComp.EventFuncName});");
                    break;
                case UIViewType.Scrollbar:
                    break;
                case UIViewType.Dropdown:
                    break;
                case UIViewType.InputField:
                    stringBuild.AppendLine($"\t\t\t{viewComp.Name}.onValueChanged.{tmplistener}(this.call.{viewComp.EventFuncName});");
                    break;
                case UIViewType.ScrollRect:
                    break;
                case UIViewType.Text:
                    break;
                case UIViewType.ToggleGroup:
                    break;
                case UIViewType.Animator:
                    break;
                default:
                    break;
            }
        }
        private void AddUIEvent(StringBuilder stringBuild, ViewComp viewComp, UIViewType type)
        {


            //非空则自动生成


            switch (type)
            {
                case UIViewType.Image:

                    break;
                case UIViewType.RawImage:
                    break;
                case UIViewType.Button:
                    if (string.IsNullOrEmpty(viewComp.EventFuncName))
                        viewComp.EventFuncName = $"On{viewComp.Name}Click";
                    stringBuild.AppendLine($"\t\tvoid {viewComp.EventFuncName}();");

                    break;
                case UIViewType.Toggle:
                    if (string.IsNullOrEmpty(viewComp.EventFuncName))
                        viewComp.EventFuncName = $"On{viewComp.Name}ValueChange";
                    stringBuild.AppendLine($"\t\tvoid {viewComp.EventFuncName}(bool value);");
                    //   Toggle toggle;
                    //   toggle.onValueChanged.AddListener
                    break;
                case UIViewType.Slider:
                    //if (string.IsNullOrEmpty(viewComp.EventFuncName))
                    //    viewComp.EventFuncName = $"On{viewComp.Name}ValueChange";
                    //stringBuild.AppendLine($"\t\tvoid {viewComp.EventFuncName}(float value);");
                    break;
                case UIViewType.Scrollbar:
                    break;
                case UIViewType.Dropdown:
                    break;
                case UIViewType.InputField:
                    if (string.IsNullOrEmpty(viewComp.EventFuncName))
                        viewComp.EventFuncName = $"On{viewComp.Name}ValueChange";
                    stringBuild.AppendLine($"\t\tvoid {viewComp.EventFuncName}(string value);");
                    break;
                case UIViewType.ScrollRect:
                    break;
                case UIViewType.Text:
                    break;
                case UIViewType.ToggleGroup:
                    break;
                case UIViewType.Animator:
                    break;
                default:
                    break;
            }
        }
        private void CreatUIEvent(StringBuilder stringBuild)
        {
            stringBuild.AppendLine($"\tpublic interface   {UIViewTools.InterfaceName} {{");
            for (int i = 0; i < ViewCompList.Count; i++)
            {

                if (ViewCompList[i].isCreat == false)
                    continue;

                if (ViewCompList[i].isUiEvent)
                {
                    UIViewType ty = (UIViewType)Enum.Parse(typeof(UIViewType), ViewCompList[i].TypeName);
                    AddUIEvent(stringBuild, ViewCompList[i], ty);
                }
            }
            stringBuild.AppendLine($"\t}}");
        }
    }



    public static class UIViewTools
    {
        public static UIViewRule UIViewRule;
        public static string Name;
        public static string InterfaceName;//接口名字
        public static UIType UIType;

        public static bool isFromUIviewbase=true;//是否继承UIViewBase
        public class ViewComp
        {

            public bool isCreat { set; get; } = true;//是否要创建

            public GameObject mGameObject { set; get; }
            public string Path { set; get; }
            public string TypeName { set; get; }
            public string Name { set; get; }
            public bool isUiEvent { set; get; } = true;
            public bool isAnimator { set; get; } = false;
            public bool isRepeatName { set; get; }//是否重复名称

            public string EventFuncName { set; get; }
        }
        public static List<ViewComp> ViewCompList = new List<ViewComp>();

        public static Dictionary<string, int> RepeatCompCountDic = new Dictionary<string, int>();



        [MenuItem("GameObject/UI/CreatViewTool", false, 10)]
        public static void CreatHotViewWindow() {

            CreatHotViewScript();
            UIViewWinows.CreatUIViewWindows();
        }

        public static void CreatHotViewScript()
        {
               string path = $"Assets/Editor/UIViewRule.asset";
            var viewRules = AssetDatabase.FindAssets("t:UIViewRule");
            if (viewRules.Length > 0)
                path = AssetDatabase.GUIDToAssetPath(viewRules[0]);
            //  AssetDatabase.LoadAllAssetRepresentationsAtPath("t:UIViewRule");
              UIViewRule = AssetDatabase.LoadAssetAtPath<UIViewRule>(path);
            ViewCompList.Clear();//清理数据
            RepeatCompCountDic.Clear();//清理数据
            Name = $"{Selection.activeGameObject.name}";
            InterfaceName = $"I{Name}CallBack";
            UIViewTools.UIType = UIType.HallUI;
            for (int i = 0; i < Selection.activeGameObject.transform.childCount; i++)
            {
                Transform transform = Selection.activeGameObject.transform.GetChild(i);
                ForeachTransform(transform.parent.name, transform, true);
            }
            //处理是否有重复值
            for (int i = 0; i < ViewCompList.Count; i++)
            {
                if (RepeatCompCountDic[ViewCompList[i].Name] > 1)
                {
                    ViewCompList[i].isRepeatName = true;
                }
            }
        }
        public static void ForeachTransform(string path, Transform transform, bool isFirst = false)
        {

            if (isFirst)
                AddComp(path, transform, isFirst);
            for (int i = 0; i < transform.childCount; i++)
            {
                string pathP = isFirst == true ? $"{transform.name}" : $"{path}/{transform.name}";
                AddComp(pathP, transform.GetChild(i));
                ForeachTransform(pathP, transform.GetChild(i));
            }
        }
        public static bool AddComp(string path, Transform transform, bool isFirst = false)
        {

            string typeName;

            if (transform.gameObject.GetComponent<Animator>() != null)
            {
                ViewComp tmpViewAnima = new ViewComp();
                tmpViewAnima.mGameObject = transform.gameObject;
                tmpViewAnima.TypeName = UIViewType.Animator.ToString();
                tmpViewAnima.Path = isFirst == true ? $"{transform.name}" : $"{path}/{transform.name}";
                tmpViewAnima.Name = $"Anima_{transform.name}";
                Debug.LogError(tmpViewAnima.Name);
                tmpViewAnima.isAnimator = true;

                if (RepeatCompCountDic.ContainsKey(tmpViewAnima.Name))
                {
                    RepeatCompCountDic[tmpViewAnima.Name]++;//计数加一
                }
                else
                {
                    RepeatCompCountDic.Add(tmpViewAnima.Name, 1);
                }

                ViewCompList.Add(tmpViewAnima);

            }


            if (IsMatchRule(transform.name, out typeName) == false)
                return false;

            #region 反射
            //初始化类型
            //Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
            //Assembly uiAbly = null;

            //for (int i = 0; i < assemblys.Length; i++)
            //{
            //    if (assemblys[i].GetName().Name == "UnityEngine.UI")
            //    {
            //        uiAbly = assemblys[i];
            //        break;
            //    }
            //}
            //Type type = null;
            //if (uiAbly != null)
            //{

            //    type = uiAbly.GetType($"UnityEngine.UI.{typeName}");
            //}
            //else
            //    type = typeof(MaskableGraphic);



            //Component component = transform.GetComponent(type);
            //if (component == null)
            //    return false;

            #endregion

            ViewComp tmpView = new ViewComp();

            tmpView.mGameObject = transform.gameObject;
            tmpView.TypeName = typeName;

            tmpView.Path = isFirst == true ? $"{transform.name}" : $"{path}/{transform.name}";
            tmpView.Name = transform.name;

            if (RepeatCompCountDic.ContainsKey(tmpView.Name))
            {
                RepeatCompCountDic[tmpView.Name]++;//计数加一
            }
            else
            {
                RepeatCompCountDic.Add(tmpView.Name, 1);
            }


            ViewCompList.Add(tmpView);
            return true;
        }
        //匹配规则
        public static bool IsMatchRule(string name, out string typeName)
        {
            bool isMatch = false;
            string tmp = null;
            for (int i = 0; i < UIViewRule.UIViewHeaders.Length; i++)
            {
                if (name.ToLower().StartsWith(UIViewRule.UIViewHeaders[i].Header))
                {
                    isMatch = true;
                    tmp = UIViewRule.UIViewHeaders[i].Compoent.ToString();
                    break;
                }
            }
            typeName = tmp;
            return isMatch;
        }
        public static UIViewType ConvertToEnum(string typeName)
        {
            UIViewType ty = (UIViewType)Enum.Parse(typeof(UIViewType), typeName);

            return ty;
        }
    }
}