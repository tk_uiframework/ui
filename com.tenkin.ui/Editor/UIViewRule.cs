﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UIViewEditor
{
    public enum UIType
    {

        /// <summary>
        /// 大厅UI
        /// </summary>
        HallUI = 1,
        /// <summary>
        /// 业务系统全屏UI
        /// </summary>
        FullScreen = 2,
        /// <summary>
        /// 业务系统半屏UI
        /// </summary>
        HalfScreen = 3,
        /// <summary>
        /// 弹窗
        /// </summary>
        PopBox = 4,
    }
    public enum UIViewType
    {
        Image = 1,
        RawImage = 2,
        Button = 3,
        Toggle = 4,
        Slider = 5,
        Scrollbar = 6,
        Dropdown = 7,
        InputField = 8,
        ScrollRect = 9,
        Text = 10,
        ToggleGroup = 11,
        Animator = 12,
        RectTransform = 13
    }


    [CreateAssetMenu(fileName = "UIViewRule", menuName = "UIViewTool/UIViewRule")]
    public class UIViewRule : ScriptableObject
    {
        public UIViewHeader[] UIViewHeaders;

        [ContextMenu("Do")]
        public void Test()
        {

            Debug.Log("Dtest");

            string[] array = Enum.GetNames(typeof(UIViewType));
            UIViewHeaders = new UIViewHeader[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                Debug.Log(array[i]);
                UIViewHeaders[i] = new UIViewHeader();
                UIViewHeaders[i].Header = array[i].Substring(0, 3).ToLower();
                UIViewHeaders[i].Compoent = (UIViewType)Enum.Parse(typeof(UIViewType), array[i]);
            }
        }
        [System.Serializable]
        public class UIViewHeader
        {
            public string Header;
            public UIViewType Compoent;
            public Type type;
        }
    }
}