﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace UIFramework
{
   public class UIState
    {
		protected UIMachine.EnterState mEnterDelegate;
		protected UIMachine.PushState mPushDelegate;
		protected UIMachine.PopState mPopDelegate;
		protected IState mStateObject;
		protected string mStateName;
		protected UIMachine mOwner;
		public UIState(IState obj, UIMachine owner, string name, UIMachine.EnterState e, UIMachine.PushState pu, UIMachine.PopState po)
		{
			mStateObject = obj;
			mStateName = name;
			mOwner = owner;
			mEnterDelegate = e;
			mPushDelegate = pu;
			mPopDelegate = po;
			
		}

		public IState StateObject
		{
			get
			{
				return mStateObject;
			}
		}
        public string StateName
		{
			get
			{
				return mStateName;
			}
		}
		
	}
}
