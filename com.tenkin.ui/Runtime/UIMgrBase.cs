﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace UIFramework
{

    public  class UIMgrBase:IState
    {
        public string StateName;

        public virtual void OnEnter(string preState)
        {
            throw new NotImplementedException();
        }

        public virtual void OnExit(string nextState, bool isDestroy)
        {
            throw new NotImplementedException();
        }

        public virtual void OnHide()
        {
            throw new NotImplementedException();
        }

        public virtual void OnUpdate()
        {
            throw new NotImplementedException();
        }
    }
}
