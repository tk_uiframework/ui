﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIFramework
{
    public class UIMachine
    {
        public delegate void EnterState(string stateName, bool isExitDestroy);
        public delegate void PushState(string stateName, string lastStateName);
        public delegate void PopState(bool isDestroy);
        protected Dictionary<string, UIState> mStates;
        protected string mEntryPoint;
        protected Stack<UIState> mStateStack;
        public UIMachine()
        {
            mStates = new Dictionary<string, UIState>();
            mStateStack = new Stack<UIState>();
            mEntryPoint = null;
        }
        public void Update()
        {
            if (CurrentState == null)
            {
                mStateStack.Push(mStates[mEntryPoint]);
                // CurrentState.StateObject.OnEnter(UINameDefine.UILogin);
            }
            CurrentState.StateObject.OnUpdate();
        }
        public void Register(string stateName, IState stateObject)
        {
            if (mStates.Count == 0)
                mEntryPoint = stateName;
            mStates.Add(stateName, new UIState(stateObject, this, stateName, Enter, Push, Pop));
        }
        public void PushInitSate(UIState stateObject)
        {
            mStateStack.Push(stateObject);
        }
        public UIState State(string stateName)
        {
            return mStates[stateName];
        }
        public void EntryPoint(string startName)
        {
            mEntryPoint = startName;
        }
        public UIState CurrentState
        {
            get
            {
                if (mStateStack.Count == 0)
                    return null;
                return mStateStack.Peek();
            }
        }


        /// <summary>
        /// 压入栈并跳转状态，退出上个状态(isDestroy)
        /// </summary>
        /// <param name="newState"></param>
        public void Enter(string stateName, bool isDestroy)
        {
            Push(stateName, Peek(stateName, isDestroy));
        }
        /// <summary>
        /// 压入栈并跳转状态，不会退出上个状态
        /// </summary>
        /// <param name="newState"></param>
        public void Push(string newState)
        {
            string lastName = null;
            if (mStateStack.Count > 1)
            {
                lastName = mStateStack.Peek().StateName;
            }
            Push(newState, lastName);
        }
        /// <summary>
        /// 从当前状态（栈 pop）切换至上一个状态(栈 peek)
        /// </summary>
        /// <param name="isDestroy"></param>
        public void Pop(bool isDestroy)
        {
            if (mStateStack.Count > 1)
            {
                UIState lastState = mStateStack.Pop();
                UIState nextState = mStateStack.Peek();
                lastState.StateObject.OnExit(nextState.StateName, isDestroy);
                nextState.StateObject.OnEnter(lastState.StateName);
            }
        }
        /// <summary>
        /// 退出所有状态
        /// </summary>
        public void ExitAll()
        {
            while (mStateStack.Count > 0)
            {
                UIState state = mStateStack.Pop();
                state.StateObject.OnExit("", true);
            }
        }
        //压入栈并跳转状态
        private void Push(string stateName, string lastStateName)
        {
            mStateStack.Push(mStates[stateName]);
            mStateStack.Peek().StateObject.OnEnter(lastStateName);
        }
        //退出当前状态，并不会pop栈顶
        private string Peek(string newName, bool isDestroy)
        {
            UIState lastState = null;
            if (mStateStack.Count > 0)
                lastState = mStateStack.Peek();
            string newState = null;
            if (newName == null && mStateStack.Count > 1)
            {
                int index = 0;
                foreach (UIState item in mStateStack)
                {
                    if (index++ == mStateStack.Count - 2)
                    {
                        newState = item.StateName;
                    }
                }

            }
            else
            {
                newState = newName;
            }
            string lastStateName = null;
            if (lastState != null)
            {
                lastStateName = lastState.StateName;

                lastState.StateObject.OnExit(newState, isDestroy);
            }
            // mStateStack.Pop();
            return lastStateName;
        }


    }
}
