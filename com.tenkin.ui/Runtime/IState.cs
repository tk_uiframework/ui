using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UIFramework
{
    public interface IState
    {
        void OnEnter(string preState);
        void OnExit(string nextState, bool isDestroy);
        void OnUpdate();
        void OnHide();
    }
}
