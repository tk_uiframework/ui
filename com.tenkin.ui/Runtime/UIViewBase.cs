﻿

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace UIFramework
{
   public abstract class UIViewBase
    {
        public abstract string PrefabsName { get; }
        public abstract UIType UIType { get; }
        public int InstanceID { get { return gameObject != null ? gameObject.GetInstanceID() : -1; } }


        public GameObject gameObject;
        private UIMgr uimgr;
        
        public void CreatGameObject(UIType uIType,GameObject go,UIMgr _uiMgr, bool isMatchParent=true)
        {
            
            gameObject= go;
            RectTransform rt = gameObject.GetComponent<RectTransform>();
            uimgr = _uiMgr;
            uimgr.SetUIType(rt, uIType, isMatchParent);
        }



        public abstract void Dispose();

        public virtual void Show()
        {
            if (gameObject != null)
            {
                gameObject.SetActive(true);
                gameObject.transform.SetAsLastSibling();
            }
        }
        /// <summary>
        /// 关闭不摧毁
        /// </summary>
        public virtual void Close()
        {
            if (gameObject != null)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
