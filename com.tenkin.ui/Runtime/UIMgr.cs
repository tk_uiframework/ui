﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

namespace UIFramework
{
    /// <summary>
    /// UI系统名称定义
    /// </summary>
    public static class UINameDefine
    {
        public const string UILogin = "UI";
        public const string UITestUI = "TestUI";
        
    }

    public enum UIType
    {
        /// <summary>
        /// 大厅UI
        /// </summary>
        HallUI = 1,
        /// <summary>
        /// 业务系统全屏UI
        /// </summary>
        FullScreen = 2,
        /// <summary>
        /// 业务系统半屏UI
        /// </summary>
        HalfScreen = 3,
        /// <summary>
        /// 弹窗
        /// </summary>
        PopBox = 4,
    }
    /// <summary>
    /// 管理系统栈和跳转系统，系统层级等
    /// </summary>
    public class UIMgr 
    {
        
        public UIMachine fsm;
        private GraphicRaycaster GraphicRaycaster;
        private RectTransform canvas;
        public RectTransform Canvas
        {
            get
            {
                if (canvas == null)
                {
                    canvas = GameObject.Find("GlobalCanvasPopUI").transform.GetComponent<RectTransform>();
                }
                return canvas;
            }
        }
        public Camera UICamera;
        //层级池
        Dictionary<UIType, Transform> m_UIParentCache = new Dictionary<UIType, Transform>();
     
       

        /// <summary>
        /// 注册所有UI系统，不会创建UI的Prefab，新加ui的话在这里注册
        /// </summary>
        public virtual void Init()
        {
            CreatUIType();
            fsm = new UIMachine();
            //系统 UI注册状态机
           // fsm.Register(UINameDefine.UILogin, new UILoginMgr());
           
            
          
        }

      

        public void OnUpdate()
        {
            if (fsm != null)
                fsm.Update();
        }
        /// <summary>
        /// 跳转到目标系统
        /// </summary>
        /// <param name="sysName">跳转到的系统名称</param>
        public void ChangeToSys(string sysName, bool isDestroy = false)
        {
            fsm.Enter(sysName, isDestroy);
        }
        /// <summary>
        /// 跳转上一个状态
        /// </summary>
        /// <param name="sysName">跳转到的系统名称</param>
        public void PopToSys(bool isDestroy)
        {
            fsm.Pop(isDestroy);
        }
        /// <summary>
        /// 重新初始化整个状态，回到登录状态
        /// </summary>
        public void ReInit()
        {
            fsm.ExitAll();
            fsm.Push(UINameDefine.UILogin);
        }
        /// <summary>
        /// 关闭/开启 UI所有点击事件
        /// </summary>
        /// <param name="isActive"></param>
        public void SetGraphicRaycaster(bool isActive)
        {
            if (GraphicRaycaster != null)
                GraphicRaycaster.enabled = isActive;
        }

        #region Tool
        public GameObject LoadUIGo(string prefabName, Transform parent)
        {
            GameObject go=null;
            go.transform.SetParent(parent);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            return go;
        }
        //动态创建层级
        private void CreatUIType()
        {
          
            GameObject.DontDestroyOnLoad(Canvas.gameObject);
            GraphicRaycaster = Canvas.GetComponent<GraphicRaycaster>();
            CanvasScaler canvasScaler = Canvas.GetComponent<CanvasScaler>();
            //适配方案 相机比 小于1.7 的都属于方平 width=1
            //  Logger.Log("Camera.main.aspect-" + Camera.main.aspect);
            SetCamMainStackUICam();
            canvasScaler.matchWidthOrHeight = Camera.main.aspect <= 1.7f ? 0 : 1;
            Array UITypeArray = Enum.GetValues(typeof(UIType));

            for (int i = 0; i < UITypeArray.Length; i++)
            {

                UIType uitype = (UIType)UITypeArray.GetValue(i);
                GameObject ui = new GameObject(uitype.ToString());
                ui.AddComponent<RectTransform>();
                ui.layer = 5;
                Transform uiParent = ui.transform;
                uiParent.SetParent(Canvas);
                m_UIParentCache[uitype] = uiParent;
                RectTransform rect = uiParent as RectTransform;
                rect.anchorMin = Vector2.zero;
                rect.anchoredPosition = Vector2.zero;
                rect.anchorMax = Vector2.one;
                rect.pivot = Vector2.one * .5f;
                rect.sizeDelta = Vector2.zero;
                rect.localScale = Vector3.one;
            }
        }
        public Vector2 WorldPosToUIPos(Vector3 transformWorld, Vector3 offset , Camera sceneCamera)
        {
            Vector2 anchaoredPos = Vector3.zero;
             Vector3 worldPos = transformWorld + offset;
            Vector3 v = sceneCamera.WorldToScreenPoint(worldPos, Camera.MonoOrStereoscopicEye.Mono);
            Vector2 pos = Vector2.zero;
            bool isRect = RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas, v, UICamera, out pos);
            if (isRect)
            {
                anchaoredPos = pos;
            }

            return anchaoredPos;
        }
        //添加ui事件 
        public  void AddUIEvent(GameObject obj, EventTriggerType eventTriggerType, UnityEngine.Events.UnityAction<BaseEventData> callback)
        {

            EventTrigger eventTrigger = obj.GetComponent<EventTrigger>();
            if (eventTrigger == null)
                eventTrigger = obj.AddComponent<EventTrigger>();

            if (eventTrigger.triggers == null)
                eventTrigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();

            EventTrigger.Entry tmp = new EventTrigger.Entry();
            tmp.eventID = eventTriggerType;
            tmp.callback.AddListener(callback);
            eventTrigger.triggers.Add(tmp);
        }
        public void SetUIType(RectTransform uiRect, UIType uiType, bool matchParent = true)
        {
            Transform par = m_UIParentCache.ContainsKey(uiType) ? m_UIParentCache[uiType] : null;
            uiRect.SetParent(par);
            uiRect.localScale = Vector3.one;
            if (matchParent)
            {
                uiRect.offsetMax = Vector2.zero;
                uiRect.offsetMin = Vector2.zero;
            }

            uiRect.anchoredPosition3D = Vector3.zero;
        }

        public void SetCamMainStackUICam() {

            if(UICamera==null)
                UICamera = Canvas.GetComponent<Canvas>().worldCamera;

            if (UICamera != null)
            {
                UniversalAdditionalCameraData universalAdditionalCameraData = Camera.main.GetUniversalAdditionalCameraData();
                universalAdditionalCameraData.cameraStack.Add(UICamera);
            }
        }
        #endregion

        public void HideCurrentState()
        {
            fsm.CurrentState.StateObject.OnHide();
        }


    }
}
